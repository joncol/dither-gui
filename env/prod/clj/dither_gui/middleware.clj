(ns dither-gui.middleware
  (:require [camel-snake-kebab.core :refer [->kebab-case-keyword]]
            [ring.logger :as logger]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]))

(defn wrap-middleware [handler]
  (-> handler
      (wrap-json-body {:keywords?    ->kebab-case-keyword
                       :bigdecimals? true})
      wrap-json-response
      (wrap-defaults (assoc-in site-defaults [:security :anti-forgery] false))
      logger/wrap-with-logger))
