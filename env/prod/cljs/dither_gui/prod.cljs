(ns dither-gui.prod
  (:require [dither-gui.core :as core]))

;; Ignore println statements in prod.
(set! *print-fn* (fn [& _]))

(core/init!)
