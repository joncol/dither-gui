(ns ^:figwheel-no-load dither-gui.dev
  (:require [devtools.core :as devtools]
            [dither-gui.core :as core]))

(devtools/install!)

(enable-console-print!)

(core/init!)
