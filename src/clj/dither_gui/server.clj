(ns dither-gui.server
  (:require [dither-gui.handler :refer [app unpack-ditherer]]
            [config.core :refer [env]]
            [ring.adapter.jetty :refer [run-jetty]])
  (:gen-class))

(defn -main [& args]
  (unpack-ditherer)
  (let [port (Integer/parseInt (or (some-> (:port env) str) "3000"))]
    (run-jetty app {:port port :join? false})))
