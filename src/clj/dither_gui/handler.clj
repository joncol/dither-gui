(ns dither-gui.handler
  (:require [clojure.data.codec.base64 :as b64]
            [clojure.java.io :as io]
            [clojure.java.shell :refer [sh]]
            [clojure.string :as str]
            [compojure.core :refer [GET POST defroutes]]
            [compojure.route :refer [not-found resources]]
            [config.core :refer [env]]
            [dither-gui.middleware :refer [wrap-middleware]]
            [hiccup.page :refer [include-js include-css html5]]
            [ring.util.response :as resp]))

(def mount-target [:div#app])

(defn- ditherer-path
  []
  (if (env :dev)
    (.getFile (io/resource "public/exe/ditherer"))
    "/tmp/ditherer"))

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css "/css/site.css" #_(if (env :dev) "/css/site.css" "/css/site.min.css"))
   [:link {:type        "text/css"
           :href        (str "https://use.fontawesome.com/releases/v5.0.10/"
                             "css/all.css")
           :rel         "stylesheet"
           :integrity   (str "sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOK"
                             "mrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg")
           :crossorigin "anonymous"}]])

(defn loading-page []
  (html5 (head)
         [:body {:class "body-container"}
          mount-target
          (include-js "/js/app.js")]))

(defn slurp-bytes
  "Slurp the bytes from a slurpable thing."
  [x]
  (with-open [out (java.io.ByteArrayOutputStream.)]
    (io/copy (io/input-stream x) out)
    (.toByteArray out)))

(defn write-bytes
  "Write the bytes to a file"
  [filename b]
  (with-open [out (io/output-stream filename)]
    (.write out b)))

(defn unpack-ditherer []
  (doseq [f ["public/exe/ditherer" "public/exe/lena_std.tif"]]
    (let [b (slurp-bytes (io/resource f))
          n (last (str/split f #"/"))]
      (write-bytes (str "/tmp/" n) b)))
  (sh "chmod" "a+x" (ditherer-path)))

(defroutes routes
  (GET "/" [] (loading-page))
  (GET "/about" []
    (loading-page))
  (GET "/dither" [args dev]
    (let [tmp-path     "/tmp/tmp.png"
          img-path-b64 (str tmp-path ".b64")]
      (let [{:keys [exit]} (apply sh (ditherer-path) "-o" tmp-path
                                  (str/split args #" "))]
        (if (zero? exit)
          (let []
            (with-open [in  (io/input-stream tmp-path)
                        out (io/output-stream img-path-b64)]
              (b64/encoding-transfer in out))
            (resp/file-response img-path-b64))
          (-> "Ditherer failed" resp/response (resp/status 500))))))
  (resources "/")
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
