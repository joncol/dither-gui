(ns dither-gui.events
  (:require [cljs.core.async :refer [<!]]
            [cljs-http.client :as http]
            [re-frame.core :as rf]
            [dither-gui.db :as db]
            [clojure.string :as str])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(rf/reg-event-db
  ::initialize-db
  (fn [_ _]
    db/default-db))

(rf/reg-event-db
  ::show-about
  (rf/path [:show-about])
  (fn [_ [_ value]]
    value))

(rf/reg-event-fx
  ::dither
  (fn [{:keys [db]} _]
    {:db      (assoc db :loading true)
     ::dither (select-keys db [:grayscale :dither-algorithm
                               :quantize-colors :quantize-color-count])}))

(rf/reg-event-db
  ::dither-success
  (fn [db [_ bytes]]
    (assoc db
           :loading false
           :dithered-image-bytes bytes)))

(rf/reg-event-db
  ::dither-error
  (fn [db [_ error]]
    (assoc db
           :error error
           :loading false)))

(rf/reg-event-db
  ::clear-error
  (rf/path [:error])
  (constantly nil))

(rf/reg-event-db
  ::set-grayscale
  (rf/path [:grayscale])
  (fn [_ [_ new-value]]
    new-value))

(rf/reg-event-db
  ::set-algorithm-dropdown-active
  (rf/path [:algorithm-dropdown-active])
  (fn [db [_ new-value]]
    new-value))

(rf/reg-event-db
  ::set-dither-algorithm
  (rf/path [:dither-algorithm])
  (fn [db [_ new-value]]
    new-value))

(rf/reg-event-db
  ::set-quantize-colors
  (rf/path [:quantize-colors])
  (fn [db [_ new-value]]
    new-value))

(rf/reg-event-db
  ::set-quantize-color-count
  (rf/path [:quantize-color-count])
  (fn [db [_ new-value]]
    new-value))

(rf/reg-fx
  ::dither
  (fn [opts]
    (go (let [args     ["-i" "lena_std.tif"]
              args     (if (:grayscale opts) (conj args "-g") args)
              args     (case (:dither-algorithm opts)
                         :bayer                    (conj args "-b")
                         :floyd-steinberg          (conj args "-f")
                         :simple-error-propagation (conj args "-s")
                         args)
              args     (if (:quantize-colors opts)
                         (conj args "-q" (:quantize-color-count opts))
                         args)
              response (<! (http/get "dither"
                                     {:query-params
                                      {:args (str/join " " args)}}))
              body     (:body response)]
          (if (:success response)
            (rf/dispatch [::dither-success body])
            (rf/dispatch [::dither-error body]))))))
