(ns dither-gui.db)

(def default-db
  {:grayscale            false
   :dither-algorithm     nil
   :quantize-colors      false
   :quantize-color-count 16})
