(ns dither-gui.views
  (:require [re-frame.core :as rf]
            [dither-gui.events :as events]
            [dither-gui.subs :as subs]))

(defn name-form [name]
  [:div.container "Name: "
   [:input.input {:type "text"
                  :value @name
                  :on-change #(rf/dispatch [::events/set-name
                                            (-> % .-target .-value)])}]])

(defn error-modal []
  (let [error (rf/subscribe [::subs/error])]
    [:div.modal
     {:class (when @error "is-active")}
     [:div.modal-background
      {:on-click #(rf/dispatch [::events/clear-error])}
      [:button.modal-close.is-large
       {:aria-label "close"
        :on-click #(rf/dispatch [::events/clear-error])}]]
     [:div.notification.is-warning
      [:div @error]]]))

(defn about-link []
  [:a
   {:href "#/"
    :on-click #(rf/dispatch [::events/show-about true])} "About"])

(defn navbar []
  [:nav.navbar {:role "navigation"
                :aria-label "main navigation"}
   [:div.navbar-brand.animated.swing
    [:h1.title "Ditherer"]]
   [:div.navbar-menu.is-active
    [:div.navbar-end
     [:div.navbar-item [about-link]]]]])

(defn result-images []
  (let [bytes (rf/subscribe [::subs/dithered-image-bytes])]
    (when @bytes
      [:img#dithered-result.animated.fadeInLeftBig
       {:src (str "data:image/png;base64," @bytes)}])))

(defn dither-button []
  (let [loading (rf/subscribe [::subs/loading])]
    [:div.button.is-info
     {:on-click #(rf/dispatch [::events/dither])
      :class (when @loading "is-loading is-disabled")} "Dither image"]))

(defn algorithm-selected [algorithm]
  (rf/dispatch [::events/set-dither-algorithm algorithm])
  (rf/dispatch [::events/set-algorithm-dropdown-active false]))

(defn algorithm-text [algorithm]
  (case algorithm
    :none                     "None"
    :bayer                    "Bayer"
    :floyd-steinberg          "Floyd-Steinberg"
    :simple-error-propagation "Simple error propagation"
    "Select algorithm"))

(defn dither-controls []
  (let [grayscale       (rf/subscribe [::subs/grayscale])
        dropdown-active (rf/subscribe [::subs/algorithm-dropdown-active])
        algorithm       (rf/subscribe [::subs/dither-algorithm])
        quantize-colors (rf/subscribe [::subs/quantize-colors])
        quant-count     (rf/subscribe [::subs/quantize-color-count])]
    [:div.columns
     [:div.column.is-two-thirds
      [:div.columns
       [:div.column.is-narrow
        [:input#grayscale-cb.is-checkradio
         {:type      "checkbox"
          :checked   @grayscale
          :on-change #(rf/dispatch [::events/set-grayscale
                                    (-> % .-target .-checked)])}]
        [:label {:for "grayscale-cb"} "Grayscale"]]
       [:div.column
        [:div.dropdown
         {:class (when @dropdown-active "is-active")}
         [:div.dropdown-trigger
          [:button.button
           {:aria-haspopup true
            :aria-controls "dropdown-menu"
            :on-click      #(rf/dispatch [::events/set-algorithm-dropdown-active
                                          (not @dropdown-active)])
            :on-blur       #(rf/dispatch [::events/set-algorithm-dropdown-active
                                          false])}
           [:span (algorithm-text @algorithm)]
           [:span.icon.is-small
            [:i.fas.fa-angle-down {:aria-hidden true}]]]]
         [:div#dropdown-menu.dropdown-menu {:role "menu"}
          [:div.dropdown-content
           (for [i [:none :bayer :floyd-steinberg :simple-error-propagation]]
             ^{:key (str i)}
             [:div
              [:a.dropdown-item
               {:href "#"
                :style {:padding "0px 15px"}
                ;; Note that this needs to be on-mouse-down instead of on-click,
                ;; since otherwise the on-blur event takes precedence.
                :on-mouse-down #(algorithm-selected i)}
               (algorithm-text i)]
              (when (= :none i)
                [:hr.dropdown-divider])])]]]]
       [:div.column.is-narrow
        [:input#quantize-cb.switch
         {
          :type     "checkbox"
          :on-click #(rf/dispatch [::events/set-quantize-colors
                                   (-> % .-target .-checked)])}]
        [:label {:for "quantize-cb"} "Quantize color count: "]
        [:input {:type      "number"
                 :disabled  (not @quantize-colors)
                 :value     @quant-count
                 :on-change #(rf/dispatch
                              [::events/set-quantize-color-count
                               (-> % .-target .-value)])}]
        [:input.slider.is-fullwidth.is-circle.is-medium.is-success
         {:type     "range"
          :disabled (not @quantize-colors)
          :min      1
          :max      64
          :value    @quant-count
          :on-input #(rf/dispatch [::events/set-quantize-color-count
                                   (-> % .-target .-value)])}]]]]
     [:div.column]]))

(defn about-screen []
  (let [show (rf/subscribe [::subs/show-about])]
    [:div.modal.animated.fadeIn
     {:class (when @show "is-active")}
     [:div.modal-background
      {:on-click #(rf/dispatch [::events/show-about false])}
      [:button.modal-close.is-large
       {:aria-label "close"
        :on-click #(rf/dispatch [::events/show-about false])}]]
     [:div.card
      [:header.card-header
       [:p.card-header-title "Ditherer"]]
      [:div.card-content
       [:p "Made by Jonas Collberg using"]
       [:p "ClojureScript \\ Clojure \\ C++ "
        [:i#pulsing-heart.fas.fa-heart.animated.pulse
         {:aria-hidden true
          :style {:color "red"}}]]]]]))

(defn main-panel []
  [:div.container.animated.fadeIn
   [navbar]
   [:div.container
    [:section.section
     [about-screen]
     [dither-controls]
     [:hr]
     [dither-button]
     [error-modal]]
    [result-images]]])
