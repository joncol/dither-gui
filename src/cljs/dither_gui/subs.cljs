(ns dither-gui.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
  ::show-about
  (fn [db _]
    (:show-about db)))

(rf/reg-sub
  ::loading
  (fn [db _]
    (:loading db)))

(rf/reg-sub
  ::error
  (fn [db _]
    (:error db)))

(rf/reg-sub
  ::grayscale
  (fn [db _]
    (:grayscale db)))

(rf/reg-sub
  ::algorithm-dropdown-active
  (fn [db _]
    (:algorithm-dropdown-active db)))

(rf/reg-sub
  ::dither-algorithm
  (fn [db _]
    (:dither-algorithm db)))

(rf/reg-sub
  ::quantize-colors
  (fn [db _]
    (:quantize-colors db)))

(rf/reg-sub
  ::quantize-color-count
  (fn [db _]
    (:quantize-color-count db)))

(rf/reg-sub
  ::dithered-image-bytes
  (fn [db _]
    (:dithered-image-bytes db)))
